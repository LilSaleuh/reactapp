import React, {useState} from 'react';
import PropTypes from 'prop-types';

/**
 * MyButton Class
 * @param {object} props props
 * @return {JSX.Element}
 */
function MyButton(props) {
  const [count, setCount] = useState(0);
  return (
    <div>
      <p>Vous avez cliqué {count} fois</p>
      <button onClick={() => setCount(count + 1)}>
        {props.label}
      </button>
    </div>
  );
};

MyButton.propTypes = {
  label: PropTypes.string,
};

export default MyButton;
