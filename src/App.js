import React from 'react';
import logo from './logo.svg';
import './App.css';
import MyButton from './MyButton.js';

/**
 * Display app
 * @return {string} HTML
 */
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Hi coders !
          Edit <code>src/App.js</code> and save to reload the page.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <MyButton class="mybutton" label={'Click On Me !'}/>
      </header>
    </div>
  );
}

export default App;
