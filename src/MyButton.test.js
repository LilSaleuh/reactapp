import {queryAllByText, render, screen} from '@testing-library/react';
import App from './App';
import MyButton from './MyButton';

test('button label', ()=> {
  const label = 'Click On Me !';
  render(<MyButton label={label} />);
  const ButtonLabel = screen.getByText(label);
  expect(ButtonLabel).toBeInTheDocument();
});

test('button click', ()=>{
  render(<MyButton label={'Click On Me !'}/>);
})
